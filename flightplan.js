const plan = require('flightplan')
const environment = require('./envparser')(process.env.NODE_ENV)

plan.target('prod', [
  {
    host: environment.SERVER_HOST,
    user: environment.SERVER_USER,
    agent: process.env.SSH_AUTH_SOCK,
    privateKey: environment.SERVER_PRIVATE_KEY
  }
])

plan.local(function(local) {
  local.log('Build')
  local.exec(`quasar build`)
  local.log('Upload files to remote hosts')
  local.exec([
    'rsync -avzu ',
    '-e "ssh -o IdentitiesOnly=yes  -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null',
    ` -i ${environment.SERVER_PRIVATE_KEY}" `,
    ' --delete',
    ` dist/spa/ `,
    `${environment.SERVER_USER}@${environment.SERVER_HOST}:${environment.SERVER_PATH}`
  ].join(''))
})
