export default {
  global: {
    appName: 'Melisa Opensource',
    subtitle: 'Made with',
    projects: 'Projectos',
    isComing: 'En desarrollo'
  },
  projects: {
    qface: {
      title: 'QFace',
      subtitle: 'PWA example style Facebook'
    },
    qairbnb: {
      title: 'QAirbnb',
      subtitle: 'PWA example style AirBnb'
    },
    qkeep: {
      title: 'QKeep',
      subtitle: 'SPA example clone Google Keep'
    },
    qspotify: {
      title: 'QSpotify',
      subtitle: 'SPA example clone Spotify'
    },
    qcard: {
      title: 'QCard',
      subtitle: 'SPA build business card'
    },
    qslido: {
      title: 'QSlido',
      subtitle: 'SPA example clone Slido App'
    },
    qwhats: {
      title: 'QWhats',
      subtitle: 'SPA example clone WhatsApp'
    },
    qcoupons: {
      title: 'QCoupons',
      subtitle: 'SPA technical test'
    },
    melisaSpreadsheets: {
      title: 'Melisa SpreadSheets',
      subtitle: 'Melisa platform application'
    },
    melisaDocs: {
      title: 'Melisa Docs',
      subtitle: 'Melisa platform application'
    },
    melisaForums: {
      title: 'Melisa Forum',
      subtitle: 'Melisa platform application'
    },
    melisaSites: {
      title: 'Melisa Sites',
      subtitle: 'Melisa platform application'
    },
    melisaEcommerce: {
      title: 'Melisa Ecommerce',
      subtitle: 'Melisa platform application'
    },
    melisaCalendar: {
      title: 'Melisa Calendar',
      subtitle: 'Melisa platform application'
    }
  }
}
