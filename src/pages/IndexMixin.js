import TrackingMixin from 'components/TrackingMixin'

export default {
  mixins: [
    TrackingMixin
  ],
  created () {
    this.trackEvent('page.home')
  },
  computed: {
    projects () {
      return [
        {
          key: 'qkeep',
          title: 'QKeep',
          subtitle: this.$t('projects.qkeep.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/qkeep/-/tree/develop'
        },
        {
          key: 'qface',
          title: 'QFace',
          image: 'https://qface.melisa.mx/statics/screens/index-mobile.png',
          subtitle: this.$t('projects.qface.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/pwa-qface/-/tree/develop'
        },
        {
          key: 'qspotify',
          title: 'QSpotify',
          subtitle: this.$t('projects.qspotify.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/qspotify/-/tree/develop'
        },
        {
          key: 'qairbnb',
          title: 'QAirbnb',
          subtitle: this.$t('projects.qairbnb.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/frontend-qairbnb'
        },
        {
          key: 'qwhats',
          title: 'QWhats',
          subtitle: this.$t('projects.qwhats.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/qwhats/-/tree/develop'
        },
        {
          key: 'qcoupons',
          title: 'QCoupons',
          subtitle: this.$t('projects.qcoupons.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/qcoupons/-/tree/develop'
        },
        {
          key: 'melisa.spreadsheets',
          title: 'Melisa SpreadSheets',
          subtitle: this.$t('projects.melisaSpreadsheets.subtitle'),
          url: 'https://gitlab.com/melisamx/melisa-pwa-spreadsheets/-/tree/develop'
        },
        {
          key: 'melisa.docs',
          title: 'Melisa Docs',
          subtitle: this.$t('projects.melisaDocs.subtitle'),
          url: 'https://gitlab.com/melisamx/melisa-pwa-docs/-/tree/develop'
        },
        {
          key: 'qslido',
          title: 'QSlido',
          subtitle: this.$t('projects.qslido.subtitle'),
          url: 'https://gitlab.com/melisa-opensource/qslido'
        }
      ]
    }
  },
  methods: {
    onClickBtnViewProject (project) {
      this.trackEvent('click.project', {
        project: project.key
      })
      if (project.is_coming) {
        return
      }
      window.open(project.url, '_blank')
    }
  }
}
