export default {
  methods: {
    trackEvent (eventName, data) {
      if (!window.mixpanel) {
        console.info('Mixpanel no loaded, ignore track event')
        return
      }
      window.mixpanel.track(eventName, data)
    }
  }
}
