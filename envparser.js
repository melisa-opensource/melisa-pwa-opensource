// https://medium.com/carbono/using-env-file-in-quasar-apps-72b56909302f
const DotEnv = require('dotenv')

module.exports = function (dev) {
  let environment = '.env'
  if (typeof process.env.NODE_ENV === 'string') {
    switch (process.env.NODE_ENV) {
      case 'local':
        environment += '.local'
        break
      case 'develop':
        environment += '.develop'
        break
      case 'develop.local':
        environment += '.develop.local'
        break
      case 'staging':
        environment += '.staging'
        break
      case 'prod':
        environment += '.prod'
    }
  } else {
    environment = dev ? '.env' : '.env.prod'
  }
  console.info(`Using file ${environment}`)
  const parsedEnv = DotEnv.config({
    path: environment
  }).parsed
  return parsedEnv
}
